#!/bin/bash
NETCDF=/p/projects/isimip/isimip/slange/counting_extremes_slim/data/out/
CUBE=senses

# uncomment below for testing on AFG and world only
#OTHERARGS='--areas AFG world'  

# the countrymasks repo, mostly for name checking (could be removed but now it is a dependency now)
# git@gitlab.pik-potsdam.de:isipedia/countrymasks.git
COUNTRYDATA=../countrymasks/country_data
#OTHERARGS='--cube-areas'

python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators drought
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators river-flood
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators tropical-cyclone
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators crop-failure
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators heatwave
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators all-events
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators confined-events
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA $OTHERARGS --indicators extensive-events

python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators drought
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators river-flood
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators tropical-cyclone
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators crop-failure
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators wildfire
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators heatwave
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators all-events
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators confined-events
python cubedata.py $CUBE --netcdf $NETCDF  --country-data $COUNTRYDATA --world-map --indicators extensive-events
