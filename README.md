# senses

Code to convert Stefan's netCDF data into json files and csv maps.

- create_json_files.sh is the main script
- cubedata.py is called by create_json_files


note: this code was originally intended for other purposes. It can certainly be simplified significantly, but it does the job.
