import os
import numpy as np
import netCDF4 as nc
import dimarray as da
import json
import logging

import yaml
#from isipedia.web import Study
#from isipedia.utils import build_url


class Variable:
    def __init__(self, name, netcdf_path, cube_path):
        """ e.g. land-area-exposed-to-drought-absolute-changes_future-projections_versus-temperature-change/
        """
        self.name = name
        self.netcdf_path = netcdf_path
        self.cube_path = cube_path
        vars(self).update(self._parse_name())
        assert name == self._buildname(**vars(self)), 'Expected: {}, Got: {}'.format(name, self._buildname(**vars(self)))
        self._init_axes()
        self.study_path = self.indicator
        self.jsonname = name

    @classmethod
    def fromparams(cls, **params):
        return cls(cls._buildname(**params))

    @staticmethod
    def _buildname(**params):
        return "{exposure}-{indicator}-{change}_{studytype}_{axis}".format(**params).replace('-_','_')

    def _parse_name(self):
        variable, studytype, axis = self.name.split('_')
        prefixes = ['land-area-exposed-to', 'population-exposed-to']
        prefix = ''
        for prefix in prefixes:
            if variable.startswith(prefix):
                variable = variable[len(prefix)+1:]
                break
        suffixes = ['absolute-changes', 'relative-changes']
        change = ''
        for suffix in suffixes:
            if variable.endswith(suffix):
                variable = variable[:-len(suffix)-1]
                change = suffix
                break

        return {'exposure':prefix, 'indicator':variable, 'change':change, 'studytype':studytype, 'axis':axis}
    
    @property
    def ncvariable(self):    
        params = vars(self).copy()
        if params['change']:
            return '{changeS}-in-{exposure}-{indicator}'.format(changeS=params.pop('change')[:-1], **params).replace('-','_')
        else:
            return '{exposure}-{indicator}'.format(changeS=params.pop('change')[:-1], **params).replace('-','_')

    def areanc(self, level, stat='mean-value'):
        params = vars(self).copy()
        axis = params.pop('axis')
        axis = 'versus-year' if axis in ['versus-timeslices', 'versus-years'] else axis
        return (self.netcdf_path +
                '/{variable}/{stat}/{variable}_future-projections_{level}_{axis}.nc'.format(
                    variable=self.ncvariable.replace('_','-'), axis=axis, level=level, stat=stat, **params))

    @property
    def gridnc(self):
        return self.areanc('grid-level')

    def jsonfile(self, area):
        return os.path.join(self.cube_path, self.study_path, area.lower(), self.jsonname+'_'+area+'.json')
        #return os.path.join(self.cube_path, self.indicator, self.studytype, area, self.name+'_'+area+'.json')

    
    @property
    def griddir(self):
        #return os.path.join(self.cube_path, self.indicator, self.studytype, 'world', 'maps', self.jsonname)
        #return os.path.join(self.cube_path, self.study.url, 'world', 'maps', self.jsonname+'_'+area+'.json')
        return os.path.join('maps', self.indicator, self.jsonname)

    def gridcsv(self, point):
        return os.path.join(self.griddir, str(point)+'.csv')


    def _init_axes_time(self, ds):
        self.climate_model_list = ds['climate_model'][:].tolist()
        self.impact_model_list = ds['impact_model'][:].tolist()
        self.climate_scenario_list = ds['ghg_concentration_scenario'][:].tolist()    
        self.years = ds['year'][:].tolist()    

        self.axes = {
          "timeslices_list": [(y-9,y+10) for y in self.years],
          "climate_scenario_list": self.climate_scenario_list,
          "climate_model_list": self.climate_model_list,
          "impact_model_list": self.impact_model_list,            
        }


    def _init_axes_temperature(self, ds):
        self.climate_model_list = ds['climate_model'][:].tolist()
        self.impact_model_list = ds['impact_model'][:].tolist()
        self.temperature_list = ds['temperature_change'][:].tolist()            

        self.axes = {
              "temperature_list": self.temperature_list,
              "climate_model_list": self.climate_model_list,
              "impact_model_list": self.impact_model_list,            
        }


    def _init_axes(self, area=None):
        with nc.Dataset(self.areanc(area or 'country-level')) as ds:
            if 'temperature' in self.axis:
                self._init_axes_temperature(ds)
            else:
                self._init_axes_time(ds)


    def __repr__(self):
        return 'Variable("{}")'.format(self.name)



class Point:
    """One dot on the line plot = one map
    """
    def __init__(self, scenario, climate_model, impact_model, slice):
        self.climate_model = climate_model.replace('multi-model-median','median')
        self.impact_model = impact_model.replace('multi-model-median','median')
        self.slice = slice
        self.scenario = scenario
        
    def __str__(self):
        return '{scenario}_{climate_model}_{impact_model}_{slice}'.format(**vars(self))
  
    def __repr__(self):
        return 'Point({scenario}, {climate_model}, {impact_model}, {slice})'.format(**{k:repr(v) 
                                                                                       for k, v in vars(self).items()})


class Area:
    """country or other area
    """
    def __init__(self, code, name=None, mask=None, geom=None, properties=None):
        self.code = code
        self.name = name
        self.mask = mask
        self.geom = geom
        self.properties = properties

    def __str__(self):
        return '{} :: {}'.format(self.code, self.name)

    def __repr__(self):
        return 'Area({}, {})'.format(self.code, self.name)


def tolist(a):
    ' replace nan with none'
    return np.ma.array(a, mask=a.mask if hasattr(a, 'mask') else np.isnan(a)).tolist()


esgf_search_url = {
    'drought': 'https://esg.pik-potsdam.de/search/isimip/?project=isimip2b&sector=water%20global',
    'river-flood': 'https://esg.pik-potsdam.de/search/isimip/?project=isimip2b&sector=water%20global',
    'crop-failure': 'https://esg.pik-potsdam.de/search/isimip/?project=isimip2b&sector=agriculture',
    'heatwave': 'https://esg.pik-potsdam.de/search/isimip/?model=IPSL-CM5A-LR,GFDL-ESM2M,MIROC5',
    'tropical-cyclone': 'https://esg.pik-potsdam.de/search/isimip',  # Cannot be provided
    'wildfire': 'https://esg.pik-potsdam.de/search/isimip/?project=isimip2b&sector=biomes',
    'default': 'https://esg.pik-potsdam.de/search/isimip',
}


class JsonData:
    
    def __init__(self, variable, area, cube, cube_std=None):
        self.variable = variable
        self.area = area
        self.cube = cube
        self.cube_std = cube_std
        self.name = variable.ncvariable.replace('_',' ')+'s'
        self.metadata = {}

        assert len(variable.climate_model_list) == cube.climate_model.size, 'climate_model mismatch'
        assert len(variable.impact_model_list) == cube.impact_model.size, 'impact_model mismatch'
        #assert len(variable.climate_scenario_list) == cube.ghg_concentration_scenario.size, 'climate_scenario mismatch'

        self._vstemp = 'temperature' in self.variable.axis

        if self._vstemp:
            self.plot_type = "indicator_vs_temperature"
            self.plot_label_x = "Global warming level"
            self.plot_unit_x = "°C"    
            self.plot_title =  self.name + ' vs. Global warming level'

        else:

            self.plot_type = "indicator_vs_timeslices"    
            self.plot_label_x = "Time Slices"
            self.plot_unit_x = ""
            self.plot_title = self.name + ' vs. Time slices'
            self.metadata.update({"n_timeslices": len(self.variable.years)})


    def header(self):
        hdr = self.metadata.copy()
        hdr.update({
             'plot_type': self.plot_type,
              "indicator": self.variable.indicator,
              "variable": self.name,
              "assessment_category": self.variable.studytype,
              "area": self.area.code,
              "region": self.area.name,
              #"esgf_search_url": "https://esg.pik-potsdam.de/esg-search/search/?offset=0&limit=10&type=Dataset&replica=false&latest=true&project=ISIMIP2b&sector=Water+Global&distrib=false&replica=false&facets=world_region%2Cvariable%2Ctime_frequency%2Clicence%2Cproduct%2Cexperiment%2Cproject%2Ccountry%2Csector%2Cimpact_model%2Cperiod%2Cbias_correction%2Cdataset_type%2Cmodel%2Cvariable_long_name%2Cco2_forcing%2Csocial_forcing%2Cirrigation_forcing%2Cvegetation%2Ccrop%2Cpft%2Cac_harm_forcing%2Cdiaz_forcing%2Cfishing_forcing%2Cmf_region%2Cocean_acidification_forcing%2Cwr_station%2Cwr_basin%2Cmelting_forcing%2Cpopulation_forcing%2Cearth_model_forcing%2Cadaptation_forcing%2Cforestry_stand",
              "esgf_search_url": esgf_search_url.get(self.variable.indicator, 'default'),
              "plot_title": self.plot_title,
              "plot_label_x": self.plot_label_x,
              "plot_unit_x": self.plot_unit_x,
              "plot_label_y": self.name,
              "plot_unit_y": "% of land area" if self.variable.exposure.startswith('land-area') else '% of population',
        })
        return hdr


    @staticmethod
    def _data_time(cube, variable, cube_std=None):       
        if cube_std is None:
            cube_std = da.zeros_like(cube)

        js = {}            
        for k, scenario in enumerate(variable.climate_scenario_list):
            js[scenario] = {}

            for i, gcm0 in enumerate(variable.climate_model_list):
                gcm = gcm0.replace('multi-model-median','overall')
                js[scenario][gcm] = {
                    'runs': {},
                }
                
                for j, impact in enumerate(variable.impact_model_list):
                    mean = cube.loc[(slice(None), scenario, gcm0, impact)].values
                    std = cube_std.loc[(slice(None), scenario, gcm0, impact)].values
                    js[scenario][gcm]['runs'][impact] = {
                        'mean': tolist(mean), 
                        'shading_upper_border': tolist(mean+std), 
                        'shading_lower_border': tolist(mean-std), 
                    }

                median = js[scenario][gcm]['runs'].pop('multi-model-median')
                median['median'] = median.pop('mean')                    
                js[scenario][gcm].update(median)
                        
        return js


    @staticmethod
    def _data_temperature(cube, variable, cube_std=None):       
        if cube_std is None:
            cube_std = da.zeros_like(cube)
        js = {}
        for i, gcm0 in enumerate(variable.climate_model_list):
            gcm = gcm0.replace('multi-model-median','overall')
            js[gcm] = {
                'runs': {},
            }
            for j, impact in enumerate(variable.impact_model_list):
                mean = cube.loc[(slice(None), gcm0, impact)].values
                std = cube_std.loc[(slice(None), gcm0, impact)].values                
                js[gcm]['runs'][impact] = {
                    'mean': tolist(mean),
                    'shading_upper_border': tolist(mean+std), 
                    'shading_lower_border': tolist(mean-std),                     
                }
            median = js[gcm]['runs'].pop('multi-model-median')
            median['median'] = median.pop('mean')
            js[gcm].update(median)

        return js

    def data(self):
        if self._vstemp:
            return self._data_temperature(self.cube, self.variable, self.cube_std)
        else:
            return self._data_time(self.cube, self.variable, self.cube_std)

    
    def todict(self):
        js = {}
        js.update(self.header())
        js.update(self.variable.axes)
        js['climate_model_list'] = [m for m in js['climate_model_list'] if m != 'multi-model-median']
        js['impact_model_list'] = [m for m in js['impact_model_list'] if m != 'multi-model-median']
        js.update({
            "data": self.data(),
        })
        return js    
  

def create_json(variable, area, level='country-level'):
    '''laod create json file for an area'''
    cube = da.read_nc(variable.areanc(level), variable.ncvariable, indices={"cor":area.code})
    cube_std = da.read_nc(variable.areanc(level, stat='standard-deviation'), variable.ncvariable, indices={"cor":area.code})
    return JsonData(variable, area, cube, cube_std)


def generate_variables(indicators, exposures, changes, axes, cube_path, netcdf_path):
    return [Variable(exposure+'-'+indicator+(('-'+change) if change else '')+'_future-projections_'+axis, 
        cube_path=cube_path, netcdf_path=netcdf_path)
                for indicator in indicators for exposure in exposures for change in changes for axis in axes]


def get_areas_netcdf_input(variable, level):
    filename = variable.areanc(level)
    if not os.path.exists(filename):
        print('WARNING: no areas for',variable,'at',level)
        return []
    with nc.Dataset(filename) as ds:
        return ds['cor'][:].tolist()


def save_array(filename, array, mask=None):
    "save csv array"
    if mask is None:
        mask = np.zeros(array.shape, dtype=bool)
    lines = [','.join([('{:e}'.format(val) if not missing else '') 
                       for val, missing in zip(canvas_line, missing_line)])
            for canvas_line, missing_line in zip(array, mask)]
    open(filename, 'w').write("\n".join(lines)) 


def create_grid_csv(v):
    ds = nc.Dataset(v.gridnc)

    if v.axis == 'versus-temperature-change':
        for i, t in enumerate(ds['temperature_change'][:]):
            for j, gcm in enumerate(ds['climate_model'][:]):
                for k, impact in enumerate(ds['impact_model'][:]):
                    p = Point('all', gcm, impact, t)
                    print(' -',p)
                    csvname = v.gridcsv(p)
                    if os.path.exists(csvname):
                        continue 
                    data = ds[v.ncvariable][i, :, :, j, k]
                    if not os.path.exists(v.griddir):
                        os.makedirs(v.griddir)
                    save_array(csvname, data, mask=data==0)

    elif v.axis == 'versus-timeslices':
        datacube = ds[v.ncvariable][:]
        years = ds['year'][:]
        climate_models = ds['climate_model'][:]
        impact_models = ds['impact_model'][:]
        scenarios = ds['ghg_concentration_scenario'][:]
        
        for i, scenario in enumerate(scenarios):
            for j, gcm in enumerate(climate_models):
                for k, impact in enumerate(impact_models):
                    for l, t in enumerate(years):
                        if t > 2100:
                            continue
                        p = Point(scenario, gcm, impact, '{}-{}'.format(t-9, t+10))
                        print(' -',p)
                        csvname = v.gridcsv(p)
                        if os.path.exists(csvname):
                            continue 
                        #data = datacubeT[k, j, i, :, :, l]
                        data = datacube[l, :, :, i, j, k]
                        if not os.path.exists(v.griddir):
                            os.makedirs(v.griddir)
                        save_array(csvname, data, mask=data==0)

    ds.close()


def main(cmd=None):

    indicators = [
        "drought",
        "heatwave",
        "river-flood",
        "tropical-cyclone",
        "crop-failure",
        "wildfire",
        "all-events",
        "confined-events",
        "extensive-events",
        ]

    axes = [
            "versus-temperature-change",
            "versus-timeslices"
        ]

    changes = [
        "",
        "absolute-changes",
        #"relative-changes"
    ]
    
    exposures = [
            "land-area-exposed-to",
            "population-exposed-to"
        ]

    NETCDF_PATH = "/p/tmp/slange/counting_extremes/cube_data" 
    #NETCDF_PATH = "counting_extremes/cube_data"
    # CUBE_PATH = "../../isipedia-data-cube/cube/" 
    #COUNTRYMASKS = "/p/projects/isipedia/perrette/countrymasks"
    #COUNTRYDATA = "/p/projects/isipedia/perrette/countrymasks"
    # MASK_FILE = "countrymasks/countrymasks.nc"
    # SHAPE_FILE = "countrymasks/countrymasks.geojson"
    # CODE_FIELD = 'ISIPEDIA'


    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--indicators', nargs='+', default=indicators, help='%(default)s')    
    parser.add_argument('--exposures', nargs='+', default=exposures, help='%(default)s')    
    parser.add_argument('--changes', nargs='+', default=changes, help='%(default)s')    
    parser.add_argument('--axes', nargs='+', default=axes, help='%(default)s')    

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--areas', nargs='+', default=None, help='all areas present in netCDF files (country-level and region-level)')    
    group.add_argument('--cube-areas', action='store_true', help="only consider areas that are defined under cube's country_data")
    group.add_argument('--world-map', action='store_true', help='create csv grid instead of area-specific json files')    

    parser.add_argument('--country-data-path', help='default: <CUBE_PATH>/country_data')    
    parser.add_argument('--skip-error', action='store_true', help='')    
    parser.add_argument('--netcdf-path', default=NETCDF_PATH, help='%(default)s')    
    parser.add_argument('cube_path', help='data cube for json file outputs')    
    # parser.add_argument('--mask-file', default=MASK_FILE, help='%(default)s')    
    # parser.add_argument('--shape-file', default=SHAPE_FILE, help='%(default)s')    
    # parser.add_argument('--code-field', default=CODE_FIELD, help='%(default)s')    

    o = parser.parse_args(cmd)

    if o.country_data_path is None:
        o.country_data_path = os.path.join(o.cube_path, 'country_data')

    variables = generate_variables(o.indicators, o.exposures, o.changes, o.axes, 
        netcdf_path=o.netcdf_path, cube_path=o.cube_path)

    if o.world_map:
        for v in variables:
            print(v.name)
            create_grid_csv(v)
        parser.exit(0)

    if o.cube_areas:
        if o.areas: logging.warning('areas overwritten by country_data areas')
        o.areas = sorted(os.listdir(o.country_data_path))

    for v in variables:
        print(v.name)
        for level in ['country-level', 'region-level']:
            all_areas = sorted(get_areas_netcdf_input(v, level))
            for area_code in all_areas:
                if o.areas is not None and area_code not in o.areas:
                    continue # skip what is not explicitly required
                defpath = os.path.join(o.country_data_path, area_code, area_code+'_general.json')
                if os.path.exists(defpath):
                    name = json.load(open(defpath))['name']
                    area = Area(area_code, name)
                else:
                    print('!!', area_code, 'not defined in country_data')
                    area = Area(area_code, area_code)
                print(area)
                try:
                    jsdata = create_json(v, area, level)
                except Exception as error:
                    if o.skip_error:
                        print('SKIP:',str(error))
                        continue
                    else:
                        raise
                js = jsdata.todict()
                dirname = os.path.dirname(v.jsonfile(area.code))
                if not os.path.exists(dirname):
                    os.makedirs(dirname)
                json.dump(js, open(v.jsonfile(area.code),'w'))


if __name__ == '__main__':
    main()
